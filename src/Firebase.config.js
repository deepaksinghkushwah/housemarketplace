// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import {getFirestore} from "firebase/firestore";

// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyC4BTlQfqb9WUC0wk9HBU1FZ3Eyd6SqGsQ",
  authDomain: "house-marketplace-app-5ee08.firebaseapp.com",
  projectId: "house-marketplace-app-5ee08",
  storageBucket: "house-marketplace-app-5ee08.appspot.com",
  messagingSenderId: "59937110343",
  appId: "1:59937110343:web:3ad98a2f4385a9cbd53462"
};

// Initialize Firebase
initializeApp(firebaseConfig);
export const db = getFirestore();
