import "./App.css";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Explore from "./pages/Explore";
import Offers from "./pages/Offers";
import Profile from "./pages/Profile";
import Signin from "./pages/Signin";
import Singup from "./pages/Singup";
import ForgotPassword from "./pages/ForgotPassword";
import Navbar from "./components/Navbar";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import PrivateRoutes from "./components/PrivateRoutes";
import Category from "./pages/Category";
import CreateListing from "./pages/CreateListing";
import Listing from "./pages/Listing";
import Contact from "./pages/Contact";
import EditListing from "./pages/EditListing";
function App() {
  return (
    <>
      <Router>
        <Routes>
          <Route path="/" element={<PrivateRoutes />}>
            <Route path="/" element={<Explore />} />
          </Route>

          <Route path="/offers" element={<Offers />} />
          <Route path="/category/:categoryName" element={<Category />} />
          <Route path="/profile" element={<PrivateRoutes />}>
            <Route path="/profile" element={<Profile />} />            
          </Route>

          <Route path="/create-listing" element={<CreateListing/>} />            
          <Route path="/edit-listing/:listingId" element={<EditListing/>} />            
          <Route path="/contact/:landlordID" element={<Contact/>} />

          <Route path="/category/:categoryName/:listingId" element={<Listing/>} />            
          
          <Route path="/sign-in" element={<Signin />} />
          <Route path="/sign-up" element={<Singup />} />
          <Route path="/forgot-password" element={<ForgotPassword />} />
        </Routes>        
        <Navbar />
      </Router>
      <ToastContainer />
    </>
  );
}

export default App;
