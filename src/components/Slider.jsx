import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader
import { Carousel } from 'react-responsive-carousel';
export const Slider = ({ imgUrls }) => {    
    let nUrls = [];
    
    

    const slider = (
        <Carousel showThumbs={false}>            
            {imgUrls.length > 0 && imgUrls.map((url, index) => (

                <div key={index}>
                    <img src={url} height="300px" width="100%" />
                </div>


            ))}
        </Carousel>
    );
    return slider;
};

