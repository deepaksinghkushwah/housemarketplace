import { useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import { ReactComponent as ArrowRightIcon } from '../assets/svg/keyboardArrowRightIcon.svg';
import visiblityIcon from '../assets/svg/visibilityIcon.svg';
import { getAuth, createUserWithEmailAndPassword, updateProfile } from 'firebase/auth';
import { db } from '../Firebase.config';
import { setDoc, doc, serverTimestamp } from 'firebase/firestore';
import { toast } from 'react-toastify';
import OAuth from '../components/OAuth';
function Signup() {
    const [showPassword, setShowPassword] = useState(false);
    const [formData, setformData] = useState({
        email: '',
        password: '',
        name: ''
    })

    const { email, password, name } = formData;

    const navigate = useNavigate();
    const onChange = (e) => {
        setformData((prevState) => ({
            ...prevState,
            [e.target.id]: e.target.value
        }));
    }
    const onSubmit = async (e) => {
        e.preventDefault();
        try {
            // create user
            const auth = getAuth();
            const userCredential = await createUserWithEmailAndPassword(auth, email, password);
            const user = userCredential.user;
            updateProfile(auth.currentUser, {
                displayName: name
            });

            // save data to firestore database
            const formDataCopy = { ...formData };
            delete formDataCopy.password; // remove password form form data copy
            formDataCopy.timestamp = serverTimestamp();
            await setDoc(doc(db, 'users', user.uid), formDataCopy);

            navigate("/");

        } catch (error) {
            toast.error("Unable to register user");
        }
    }
    return (
        <>
            <div className="container">
                <header>
                    <p className='pageHader'>Welcome Back</p>
                </header>
                <form onSubmit={onSubmit}>
                    <input type="text" className='nameInput' name="name" id="name" value={name} onChange={onChange} placeholder='Name' />

                    <input type="email" className='emailInput' name="email" id="email" value={email} onChange={onChange} placeholder='Email' />

                    <div className="passwordInputDiv">
                        <input type={showPassword ? 'text' : 'password'} name="password" id="password" className='passwordInput' placeholder='Password' value={password} onChange={onChange} />

                        <img src={visiblityIcon} className='showPassword' alt="Show Password" onClick={() => setShowPassword((prevState) => !prevState)} />
                    </div>

                    <Link to="/forgot-password" className='forgotPasswordLink'>Forgot Password</Link>

                    <div className="signUpBar">
                        <p className="signUpText">Sign Up</p>
                        <button className='signUpButton'>
                            <ArrowRightIcon fill="#ffffff" width="34px" height="34px" />
                        </button>
                    </div>
                </form>
                <OAuth/>
                <Link to="/sign-in" className='registerLink'>Sign In Instead</Link>
            </div>
        </>
    )
}

export default Signup
