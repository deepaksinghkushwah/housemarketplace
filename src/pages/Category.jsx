import { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { collection, getDoc, query, where, orderBy, limit, startAfter, getDocs } from 'firebase/firestore';
import { db } from '../Firebase.config';
import { toast } from 'react-toastify';
import Spinner from '../components/Spinner';
import ListingItem from '../components/ListingItem';


function Category() {
    const [listings, setListings] = useState(null);
    const [loading, setLoading] = useState(true);
    const params = useParams();
    const [lastFetchedListing, setLastFetchedListing] = useState(null);

    useEffect(() => {
        const fetchListings = async () => {
            try {
                // get ref
                const listingRef = collection(db, 'listings');

                // create query
                const q = query(listingRef, where('type', '==', params.categoryName), orderBy('timestamp', 'desc'), limit(10));

                // execute query
                const querySnap = await getDocs(q);

                const lastVisible = querySnap.docs[querySnap.docs.length - 1];
                setLastFetchedListing(lastVisible);

                const listings = [];
                querySnap.forEach((doc) => {
                    return listings.push({
                        id: doc.id,
                        data: doc.data()
                    });
                });

                setListings(listings);
                setLoading(false);
            } catch (error) {
                toast.error("Error at loading listings");
            }
        }
        fetchListings();




    }, [params.categoryName]);

    // pagination /load more
    const onFetchMoreListings = async () => {
        try {
            // get ref
            const listingRef = collection(db, 'listings');

            // create query
            const q = query(listingRef, where('type', '==', params.categoryName), orderBy('timestamp', 'desc'), startAfter(lastFetchedListing), limit(10));

            // execute query
            const querySnap = await getDocs(q);

            const lastVisible = querySnap.docs[querySnap.docs.length - 1];
            setLastFetchedListing(lastVisible);

            const listings = [];
            querySnap.forEach((doc) => {
                return listings.push({
                    id: doc.id,
                    data: doc.data()
                });
            });

            setListings((prevState) => [...prevState, ...listings]);
            setLoading(false);
        } catch (error) {
            toast.error("Error at loading listings");
        }
    }







    return (
        <div className='category'>
            <header>
                <p className="pageHeader">Places for {params.categoryName === 'rent' ? 'Rent' : 'Sale'}</p>
            </header>
            {loading ? <Spinner /> : listings && listings.length > 0 ? <>
                <main>
                    <ul className="categoryListings">
                        {listings.map((listing) => (
                            <ListingItem key={listing.id} listing={listing.data} id={listing.id} />
                        ))}
                    </ul>
                </main>

                <br />
                <br />
                {lastFetchedListing && (
                    <p className="loadMore" onClick={onFetchMoreListings}>
                        Load More
                    </p>
                )}

            </> : <p>No listings for {params.categoryName}</p>}
        </div>
    )
}

export default Category
