import { useState, useEffect } from 'react';
import { useParams, useSearchParams } from 'react-router-dom';
import { doc, getDoc } from 'firebase/firestore';
import { db } from '../Firebase.config';
import { toast } from 'react-toastify';



function Contact() {
    const [message, setMessage] = useState('');
    const [landlord, setLandLord] = useState(null);
    const [searchParams, setSearchParams] = useSearchParams();

    const params = useParams();

    useEffect(() => {
        const getLandLord = async () => {
            const docRef = doc(db, 'users', params.landlordID);
            const docSnap = await getDoc(docRef);
            if(docSnap.exists()){
                setLandLord(docSnap.data());
            } else {
                toast.error("Could not get landlord data");
            }
        }

        getLandLord();
    }, [params.landlordID]);

    const onChange = (e) => {
        setMessage(e.target.value);
    }


    return <div className='pageContainer'>
        <header>
            <p className="pageHeader">Contact Landlord</p>

            {landlord !== null && (
                <main>
                    <div className="contactLandlord">
                        <p className="landlordName">Contact {landlord?.name}</p>
                    </div>
                    <form className="messageform">
                        <div className="messageDiv">
                            <label htmlFor="message" className="messageLabel">Message</label>
                            <textarea name="message" className='textarea' value={message} onChange={onChange} id="message" cols="30" rows="10"></textarea>
                        </div>

                        <a href={`mailto:${landlord.email}?Subject=${searchParams.get('listingName')}&body=${message}`}>
                            <button type='button' className="primaryButton">Send Message</button>
                        </a>
                    </form>
                </main>
            )}
        </header>
        </div>;
}

export default Contact;
